import pprint
import requests
import json
import sys

config = {}
access_token = ''
classification = ''
org = ''
endpoint = ''
updateRecord = True
organisationId = 100

def loadConfig():
    try:
        with open ('../FieldDefinitions/config.json', 'r') as f:
            return json.load(f)
    except:
        print('Error loading config file!')
        exit()

def getToken():

    url = '{}/auth/ropc.php'.format(endpoint)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {"grant_type" : "password" , "client_id" : config['parameters']['client_id'], "client_secret" : config['parameters']['client_secret'], "username" : config['parameters']['username'], "password": config['parameters']['password']}
    response = requests.request('POST', url, headers=headers, data=data)
    
    if response.status_code != 200:
        raise Exception('Failed getting token, {} - {}!'.format(response.status_code, response.text))
    return response.json()['access_token']

def getCurrentUser():

    print('----> Getting current user info:')

    url = '{}/mediahaven-rest-api/v2/users/current'.format(endpoint)
    headers = {'Content-Type' : 'application/json', 'Authorization' : 'bearer' + access_token}
    response = requests.request('GET', url, headers=headers)

    if response.status_code != 200:
        raise Exception('-----> Failed to get current user! {} - {}'.format(response.status_code, response.text))
    else:
        print('-----> Successful')
        return response.json()

def updateProfiles(profiles, recordId):

    type = 'Record' if updateRecord else 'Collection'
    print("--> Updating {} profiles for {} classification.".format(type, classification))
    testProfiles = ['785fefad-18b4-40a9-a114-a7f7ff1b409f', '4c1e1cbd-9d61-41e2-8101-ea42577f6364', 'a04ae5ee-dee6-434b-a9fe-1d732279676c', 'b5fe8a26-0ee9-488e-92d9-2233f429e495', 'c7340acd-a96d-4198-add5-2d3272005b84']
    
    if updateRecord:
        if not profiles:
            profiles['Record'] = []
        newProfiles = profiles['Record']
        for profile in testProfiles:
            newProfiles.append(profile)
        url = '{}/mediahaven-rest-api/v2/records/{}/profiles'.format(endpoint, recordId)
        headers = {'Content-Type': 'application/json', 'Authorization': 'bearer' + access_token}
        data = {"Profiles" : newProfiles}
        response = requests.request('PUT', url, headers=headers, data=json.dumps(data))
        if response.status_code != 204:
            raise Exception('Failed to update profiles for {} classification, {} - {}'.format(classification, response.status_code, response.text))
        else:
            print('---> Successful!')

def createNewClassification():
    print('--> Creating new classification {}'.format(classification))
    url = '{}/mediahaven-rest-api/v2/records'.format(endpoint)
    headers = {'Content-Type': 'application/json', 'Authorization': 'bearer ' + access_token}
    data = {"RecordType": "Classification", "Title": classification, "IngestSpaceId": "aa{}ce7-51d6-41db-bf44-b085a4464c22".format(str(organisationId))}
    response = requests.request('POST', url, headers=headers, data=json.dumps(data))
    if response.status_code != 201:
        print('---> Creation failed! {} - {}'.format(response.status_code, response.text))
        raise Exception('Failed adding classification {}! {} - {}'.format(classification, response.status_code, response.text))
    else:
        print('---> Creation successful!')
        fragmentId = response.json()['Internal']['FragmentId']

    readPerm = []
    writePerm = []

    userInfo = getCurrentUser()
    for group in userInfo['Groups']:
        if group['Type'] == 'Administrator':
            writePerm.append(group['Id'])
        readPerm.append(group['Id'])

    print('---> Setting default rights:')
    metadata = {'Metadata':{'MergeStrategies':{'Permissions': 'OVERWRITE'}, "RightsManagement": {'Permissions': {'Read' : readPerm, 'Export': [], 'Write': writePerm}}}}
    url = '{}/mediahaven-rest-api/v2/records/{}'.format(endpoint, fragmentId)
    headers = {'Content-Type': 'application/json', 'Authorization': 'bearer' + access_token}

    response = requests.request('POST', url, headers=headers, data=json.dumps(metadata))
    if response.status_code != 201 and response.status_code != 200:
        print('----> Failed: Rights not correctly set. {} - {}'.format(response.status_code, response.text))
    else:
        print('----> Successful!')

def createClassification():

    print("Checking if {} is an existing classification:".format(classification))
    url = '{}/mediahaven-rest-api/v2/records?q=%2B(Title:"{}")%20%2B(RecordType:Classification)%20%2B(IsInIngestspace:*)%20%2B(OrganisationName:"{}")'.format(endpoint, classification, org)
    headers = {'Content-Type': 'application/json', 'Authorization': 'bearer ' + access_token}
    response = requests.request('GET', url, headers=headers)

    if response.status_code != 200:
        raise Exception('Failed to query classifications, {} - {}!'.format(response.status_code, response.text))

    if response.json()['TotalNrOfResults'] == 0:
        print('-> {} is not an existing classification!'.format(classification))
        createNewClassification()
        createClassification()
    else:
        print('-> Classification exists:')
        profiles = response.json()['Results'][0]['Internal']['Profiles']
        recordId = response.json()['Results'][0]['Internal']['RecordId']
        updateProfiles(profiles, recordId)

if __name__ == "__main__":
    config = loadConfig()
    endpoint = 'https://{}'.format(config['parameters']['endpoint'])
    access_token = getToken()
    print('Access token: ',access_token)
    classification = sys.argv[1]
    org = sys.argv[2]
    createClassification()
