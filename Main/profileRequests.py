import requests
import json

class Profiles:
    TOKEN = ""
    profiles = {}
    
    def __init__(self, token, orgId, config, profilesDict) -> None:
        global profiles, TOKEN
        self.orgId = orgId
        self.config = config
        self.profileIds = []
        TOKEN = token
        profiles = profilesDict

    #API request to make the profile 
    def createProfile(self, profile):
        global PROFILEID
        URL = "https://{}/mediahaven-rest-api/v2/profiles".format(self.config["parameters"]["endpoint"])
        headers = {'Authorization': 'bearer ' + TOKEN}
        r = requests.post(url=URL, headers=headers, data=profile)

        if r.status_code == 201:
            response = json.loads(r.content)
            self.profileIds.append(response['Id'])
            return response['Id']
        else:
            raise Exception("Failed to add profile! {} - {}".format(r.status_code, r.text, profile))

    #Define on which recordtype the profile needs to be set
    def defineProfileType(self, profileName):
        if profiles[profileName]["Type"] == "Beide":
            return ["Record", "Mh2Collection"]
        elif profiles[profileName]["Type"] == "Record":
            return ["Record"]
        else:
            return ["Mh2Collection"]
    
    #Transform the dictionary structure to proper Profile structure for the API request
    def transformJSON(self, profile, profileName):
        file = dict()
        file = {"Names": [{
                "Lang":  "fr_BE",
                "Value": profileName
                },
            {
            "Lang": "en_US",
                "Value": profileName
        },
            {
            "Lang": "nl_BE",
                "Value": profileName
        }
        ],
            "OrganisationId": self.orgId,
            "Fields": [],
            "RecordTypes": self.defineProfileType(profileName),
        }
        fieldDef = []
        for data in profile:
            if profile[data]["parent"] == "":
                fieldDict = {
                    "FieldDefinitionId": profile[data]["fieldDefinitionId"],
                    "Required": profile[data]["required"],
                    "ReadOnly": profile[data]["readonly"],
                    "Labels": [
                        {
                            "Lang":  "fr_BE",
                            "Value": profile[data]["labels"]["fr_BE"]
                        },
                        {
                            "Lang": "en_US",
                            "Value": profile[data]["labels"]["en_US"]
                        },
                        {
                            "Lang": "nl_BE",
                            "Value": profile[data]["labels"]["nl_BE"]
                        }
                    ],
                    "Descriptions": [
                        {
                            "Lang":  "fr_BE",
                            "Value": profile[data]["descriptions"]["fr_BE"]
                        },
                        {
                            "Lang": "en_US",
                            "Value": profile[data]["descriptions"]["en_US"]
                        },
                        {
                            "Lang": "nl_BE",
                            "Value": profile[data]["descriptions"]["nl_BE"]
                        }
                    ],
                    "Public" : profile[data]["portal"]
                }
                if profile[data]["descriptions"]["nl_BE"] == None:
                    del fieldDict['Descriptions']
                    
                fieldDef.append(fieldDict)
        file['Fields'] = fieldDef
        return file

    def makeProfiles(self):
        for profile in profiles:
            file = self.transformJSON(profiles[profile]["Profile"], profile)
            Id = self.createProfile(json.dumps(file))
            print(f"Added profile {profile} with ID: {Id}")
        return self.profileIds

