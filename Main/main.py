import profileRequests
import createFieldDefinitions
import createClassification
import json
import sys
import requests
import argparse
import openpyxl
import os

TOKEN = ""
config = {}

def parseArgv(argv):
    """
    Parse parameters and validate
    """
    cmd = os.path.basename(argv[0])
    parser = argparse.ArgumentParser(
        description='Make a classification from Excel-template.',
        usage='{0} [--from path_input_file] [--org organisation_name] [--prefix fieldDefinitionsPrefix]]\n'
              '(example: "{1} --from ExampleTemplateClassification.xlsx --org stage --prefix STG)'.format(
            cmd, cmd)
    )
    parser.add_argument("--from",
                        dest = "input_file",
                        type=str,
                        help="Local path to the Excel-template",
                        required=True)
    parser.add_argument("--org",
                        dest='org',
                        type=str,
                        help="Name of the organisation for the classification",
                        required=True)
    parser.add_argument("--prefix",
                        dest='prefix',
                        type=str,
                        help="The prefix for the field definitions.",
                        required=True)
    args = parser.parse_args()
    if not validate_file(args.input_file):
        sys.exit(1)
    return args

#Check if Excelfile exists and is the right filetype
def validate_file(input_file):
    if not os.path.exists(input_file):
        print('The input file {0} does not exists'.format(input_file))
        return False
    elif os.path.splitext(input_file)[1] != '.xlsx':
        print ('the input file {0} is not supported. Allowed extensions are: {1}'.format(input_file, '.xlsx'))
        return False
    else:
        return True

#Get a token to use in the other parts of the script
def getToken():
    url = 'https://{}/auth/ropc.php'.format(config["parameters"]["endpoint"])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {"grant_type" : "password" , "client_id" : config['parameters']['client_id'], "client_secret" : config['parameters']['client_secret'], "username" : config['parameters']['username'], "password": config['parameters']['password']}
    response = requests.request('POST', url, headers=headers, data=data)
    
    if response.status_code != 200:
        raise Exception('Failed getting token, {} - {}!'.format(response.status_code, response.text))
    return response.json()['access_token']

#Load in the Excelfile
def process_excel(excelFile):
    try:
        wb = openpyxl.load_workbook(excelFile)
        return wb
    except(Exception) as error:
        print(f"Error loading Excel file: {excelFile}")
        print(error)

if __name__ == "__main__":
    try:
        with open('config.json', 'r') as f:
            config = json.load(f)
    except (Exception) as error:
        print(f"Failed to load config.json: {error}")
    args = parseArgv(sys.argv)
    TOKEN = getToken()
    excelFile = process_excel(args.input_file)
    if excelFile is not None:
        for worksheet in excelFile.sheetnames:
            if(excelFile[worksheet].sheet_state == "visible"): #Don't handle the hidden worksheets
                print("Handling worksheet: " + worksheet)
                print("---------------------------------")
                ws = excelFile[worksheet]
                FD = createFieldDefinitions.FieldDefinitions(TOKEN, config, ws, args.prefix.upper(), args.org)
                profiles = FD.fieldDefinitions()
                classificationName = FD.classification
                PR = profileRequests.Profiles(TOKEN, FD.orgId, config, profiles)
                profileIds = PR.makeProfiles()
                C = createClassification.CreateClassification(config, FD.orgId, TOKEN, profileIds, classificationName, args.org)
                C.updateOrCreateClassification()
                profiles.clear()
                print("---------------------------------")