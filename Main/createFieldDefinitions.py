import json
import os
import re
import sys
import openpyxl
import psycopg2
import warnings
import requests

class FieldDefinitions:
    profiles = {}
    baseColumns = ["field_definition_id", "parent_definition_id", "key", "type", "index",
               "global", "advanced_search", "read_only", "index_name", "possible_values", "inheritance"]
    requiredColumns = [0, 1, 3, 6, 11, 13, 14, 16, 17, 18, 19]
    childsRequired = ['Trefwoorden', 'Multi select',
                 'Multi-item', 'Herhalend veld']
    index = 3

    def __init__(self, accessToken, config, worksheet, prefix, organisation):
        self.accessToken = accessToken
        self.config = config
        self.worksheet = worksheet
        self.orgId = 0
        self.classification = ""
        self.prefix = prefix
        self.organisation = organisation

    #HELPER methods

    #Connect to the database with credentials from config.json 
    def dbConnect(self):
        try:
            conn = psycopg2.connect(database=self.config['database']['dbname'], user=self.config['database']['user'], password=self.config['database']['password'] , host=self.config['database']['host'], port=self.config['database']['port'])
            conn.autocommit = False
            print('Connected to database!')
            return conn
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            exit()

    #Method for changing the key of field_definitions to new naming convention (Prefix + label)
    def camelcase(self, string):
        return re.sub(r"(_|-)+", " ", string).title().replace(" ", "").replace("(", "").replace(")", "").replace("/", "").replace("\\", "").replace("'", "")

    #Parse the values from the Excelfile to booleans
    def parseBool(self, string):
        return True if string.upper().strip() in ['JA', 'READ-ONLY'] else False

    #Get the organisation ID based on the organisation given as a parameter from main.py
    def getOrgId(self, cursor):
        organisation = self.organisation
        try:
            cursor.execute("SELECT organisation_id FROM organisations WHERE name = %s;", [organisation])
            result = cursor.fetchone()
            if result is None:
                print(f"Failed to get organisation_id for organisation: {organisation}.")
                exit()
            return result[0]
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
            exit()

    #Get the field_definition_id linked to the given flatkey from the Excelfile
    def getFlatkeyId(self, cursor, flatkey):
        id = None
        try:
            cursor.execute('SELECT field_definition_id FROM field_definitions WHERE key = %s AND parent_definition_id IS NULL', [flatkey])
            id = cursor.fetchone()[0]
        except(Exception, psycopg2.DatabaseError) as error:
            print(f'Error getting id for standardfield with flatkey {flatkey}')
            print(error)
            exit()
        return id

    #Creating Field definitions

    #Parse the field options to valid field_definition_types
    def determineFieldtype(self, type):
        if type.lower().strip() == "vrije tekst":
            return "SimpleField"
        if type.lower().strip() == "tekstvak":
            return "TextField"
        if type.lower().strip() == "single select":
            return "EnumField"
        if type.lower().strip() in ["multi select", "herhalend veld", "trefwoorden"]:
            return "ListField"
        if type.lower().strip() == "multi-item":
            return "MultiItemField"
        if type.lower().strip() == "datum":
            return "DateField"
        if type.lower().strip() == "checkbox":
            return "BooleanField"
        if type.lower().strip() == "getal":
            return "LongField"
        if type.lower().strip() == "timecode":
            return "TimeCodeField"
        if type.lower().strip() == "edtf-datum":
            return "EDTFField"
        return "Unknown"

    #Perform checks on the rows of the Excelfile to make sure no issues are present
    def rowValidation(self, row, profileName):
        errorMessage = ''
        valid = True
        for column in self.requiredColumns:
            if row[column] is None:
                valid = False
                errorMessage = "is missing required information!"
        if row[11] in ["Trefwoorden", "Multi-item", "Multi select", "Herhalend veld", "Single select"] and row[6] != 'Ja':
            if row[12] == None:
                valid = False
                errorMessage = "is missing child values!"
        if self.determineFieldtype(row[11]) == 'Unknown':
            valid = False
            errorMessage = "has an unknown field definition type!"
        if row[6] == "Ja" and (row[7] is None or row[7] == ''):
            valid = False
            errorMessage = "is a standard field but has no flatkey!"
        if self.profiles[profileName]["Type"] in ["Beide", "Collectie"] and row[13] == "Read-only" and row[15] == "Ja":
            valid = False
            errorMessage = "field for collection can't be Read-only and required!"
        return (valid, errorMessage)

    #Get the 3 different languages from the labels or description starting from a given column 
    def getLabels(self, row, startIndex):
        dict = {'nl_BE' : '', 'fr_BE' : '', 'en_US' : ''}
        for item in dict.keys():
            dict[item] = row[startIndex]
            if row[startIndex] is None:
                if dict['nl_BE']:
                    dict[item] = dict['nl_BE']
            startIndex += 1
        return dict

    #Get all the values from a row for further use in the script
    def getValues(self, row):
        values = {}
        values['parent'] = ''
        values['fieldDefinitionId'] = ''
        values['labels'] = self.getLabels(row, 3)
        values['standard'] = self.parseBool(row[6])
        values['descriptions'] = self.getLabels(row, 8)
        values['type'] = row[11]
        values['possible_values'] = row[12]
        values['readonly'] = self.parseBool(row[13])
        values['inheritance'] = "'Propagation'" if row[14] == 'Ja' else 'NULL'
        values['required'] = self.parseBool(row[15])
        values['global'] = self.parseBool(row[16])
        values['advanced'] = self.parseBool(row[17])
        values['filter'] = self.parseBool(row[18])
        values['portal'] = self.parseBool(row[19])
        values['index'] = True if (values['global'] == True or values['advanced'] == True or values['filter'] == True) else False
        values['flatkey'] = row[7] if row[7] is not None or row[7] != '' else ''
        return values

    #Make the insert query based on the values and perform the insert returning the created field_definition_id
    def insertFieldDefinition(self, values, cursor):
        prefix = self.prefix
        organisation = self.organisation
        fieldId = None
        columns = ", ".join(self.baseColumns)
        id = "(SELECT MAX(field_definition_id) + 1 from field_definitions)"
        parentId = "NULL" if values['parent'] == '' else f"(SELECT field_definition_id from field_definitions WHERE key = '{prefix + self.camelcase(values['parent'])}')"
        key = f"'{(prefix + self.camelcase(values['labels']['nl_BE'])).strip()}'"
        type = f"'{values['type']}'"
        index = f"{values['index']}".upper()
        global_search = f"{values['global']}".upper()
        advanced_search = f"{values['advanced']}".upper()
        read_only = f"{values['readonly']}".upper()
        index_name = f"'{organisation}'"
        possible_values = "'{" + values['possible_values'] + "}'" if values['possible_values'] is not None else "'{}'"
        inheritance = f"{values['inheritance']}"
        customValues = [id, parentId, key, type, index, global_search, advanced_search, read_only, index_name, possible_values, inheritance]
        #This part can be replaced by API calls for creating field definitions when ready
        query = "INSERT INTO field_definitions (" + columns + ") VALUES (" + ", ".join(customValues) + ") RETURNING field_definition_id;"
        try:
            cursor.execute(query)
            fieldId = cursor.fetchone()[0]
            if fieldId is None:
                print(f"Failed to get field_definition_id for new field definition {key}")
                exit()
        except(Exception, psycopg2.DatabaseError) as error:
            print(f"Failed to insert field_definition: {key}")
            print(error)
            exit()
        self.insertTranslations(values['labels'], cursor)
        return fieldId

    #For a field_definition add all translations into the correct table
    def insertTranslations(self, labels, cursor):
        prefix = self.prefix
        base_label = labels['nl_BE']
        for key in labels:
            if not labels[key]:
                labels[key] = base_label
            #This part can be replaced by API calls for creating field definitions when ready
            query = "INSERT INTO field_translations VALUES ((SELECT field_definition_id from field_definitions WHERE key =  %s), %s, %s);"
            values = (prefix + self.camelcase(labels['nl_BE']), key, labels[key])
            try:
                cursor.execute(query, values)
            except(Exception, psycopg2.DatabaseError) as error:
                print(f"Failed to insert translation for {base_label}")
                print(error)
                exit()

    #Set the common values for child field_definitions
    def childFieldsCommonValues(self, currentProfile, type, key, cursor, parentLabel):
        self.profiles[currentProfile]["Profile"][key]['parent'] = parentLabel
        self.profiles[currentProfile]["Profile"][key]['labels'] = {'nl_BE': key, 'fr_BE': key, 'en_US': key}
        self.profiles[currentProfile]["Profile"][key]['standard'] = False
        self.profiles[currentProfile]["Profile"][key]['descriptions'] = ''
        self.profiles[currentProfile]["Profile"][key]['type'] = type
        self.profiles[currentProfile]["Profile"][key]['inheritance'] = 'NULL'
        self.profiles[currentProfile]["Profile"][key]['filter'] = False
        self.profiles[currentProfile]["Profile"][key]['portal'] = False
        self.profiles[currentProfile]["Profile"][key]['fieldDefinitionId'] = self.insertFieldDefinition(self.profiles[currentProfile]["Profile"][key], cursor)

    #Depending on the type of parentField, some child fields need different handling
    def addChildFields(self, parentField, parentLabel, type, currentProfile, cursor):
        if type == "EnumField":
            key = parentLabel + " Item";
            self.profiles[currentProfile]["Profile"][key] = parentField.copy()
            self.profiles[currentProfile]["Profile"][key]["possible_values"] = parentField["possible_values"]
            self.childFieldsCommonValues(currentProfile, type, key, cursor, parentLabel)
        else:
            for item in parentField['possible_values'].strip().split(","):
                key = parentLabel + " " + item.strip() 
                self.profiles[currentProfile]["Profile"][key] = parentField.copy()
                self.profiles[currentProfile]["Profile"][key]['possible_values'] = ''
                self.childFieldsCommonValues(currentProfile, type, key, cursor, parentLabel)

    #ParentFields need their options to be false since child items inherit these options themselves
    def addParentField(self, profile, type, cursor):
        profile['possible_values'] = ''
        profile['global'] = False
        profile['advanced'] = False
        profile['readonly'] = False
        profile['index'] = False
        profile['type'] = self.determineFieldtype(type)
        profile['fieldDefinitionId'] = self.insertFieldDefinition(profile, cursor)
   
    #Refresh all field definitions after succesfully inserting all new field_definitions
    def reIndex(self):
        endpoint = self.config["parameters"]["endpoint"]
        if self.accessToken != "":
            url = "https://{}/mediahaven-rest-api/v2/fielddefinitions/refreshAll".format(endpoint)
            headers = {'Authorization': 'bearer ' + self.accessToken}
            r = requests.post(url=url, headers=headers)
            if r.status_code == 202:
                print("Refreshing field definitions!")
            else:
                print("Refreshing failed: {} - {}".format(r.status_code, r.text))
                exit()
        else:
            print("No access_token to perform requests")
            exit()

    #Main part of the script where the iteration over rows happens
    def processData(self, connection, worksheet):
        global index, orgId
        prefix = self.prefix
        with connection: 
            with connection.cursor() as cursor: #Every execute now happens inside a transaction
                self.orgId = self.getOrgId(cursor)
                for row in worksheet.iter_rows(min_row=3, max_col=20, values_only=True):

                    #Add the new profile if it does not exist in the dictionary
                    profileName = row[1].strip()
                    if profileName not in self.profiles.keys():
                        self.profiles[profileName] = {"Profile": {}, "Type" : row[2].strip()}

                    #If the row contains all necessary values, get all the values from the row
                    if self.rowValidation(row, profileName)[0]:
                        self.profiles[profileName]["Profile"][row[3]] = self.getValues(row)

                        #Get a copy of the profile so it can be used for possible child fields
                        parentCopy = self.profiles[profileName]["Profile"][row[3]].copy()
                        type = self.profiles[profileName]["Profile"][row[3]]['type']

                        #Determine if the field definition needs child fields 
                        if type in self.childsRequired and row[6].upper().strip() != 'JA':
                            self.addParentField(self.profiles[profileName]["Profile"][row[3]], type, cursor)
                            if type == "Multi select":
                                self.addChildFields(parentCopy, row[3].strip(), 'EnumField', profileName, cursor)
                            if type in ["Trefwoorden", "Herhalend veld", "Multi-item"]:
                                self.addChildFields(parentCopy, row[3].strip(), 'SimpleField', profileName, cursor)

                        #Handling of copying an existing field definitions        
                        elif row[6].strip() == 'Ja':
                            self.profiles[profileName]["Profile"][row[3]]['fieldDefinitionId'] = self.getFlatkeyId(cursor, self.profiles[profileName]["Profile"][row[3]]['flatkey'])
                            self.profiles[profileName]["Profile"][row[3]]['type'] = self.determineFieldtype(type)
                        
                        else:
                            self.profiles[profileName]["Profile"][row[3]]['type'] = self.determineFieldtype(type)
                            self.profiles[profileName]["Profile"][row[3]]['fieldDefinitionId'] = self.insertFieldDefinition(self.profiles[profileName]["Profile"][row[3]], cursor)
                        self.index += 1 #Index to keep the row count for proper error indication
                    else:
                        print(f'Row {self.index} ' + self.rowValidation(row, profileName)[1])
                        exit()
        connection.close()
        print("--> All inserts OK!")
        print("Database connection closed!")

    def fieldDefinitions(self):
        self.classification = self.worksheet["A3"].value
        print("Classification: "  + self.classification)
        connection = self.dbConnect()
        self.processData(connection, self.worksheet)
        self.reIndex()
        return self.profiles