import pprint
import requests
import json
import time

class CreateClassification:

    def __init__(self, config, orgId, accessToken, profileIds, classification, organisation):
        self.config = config
        self.orgId = orgId
        self.accessToken = accessToken
        self.profileIds = profileIds
        self.classification = classification
        self.organisation = organisation

    #Get the current user to get the permissions for the classification
    def getCurrentUser(self):

        print('----> Getting current user info:')

        url = 'https://{}/mediahaven-rest-api/v2/users/current'.format(self.config["parameters"]["endpoint"])
        headers = {'Content-Type' : 'application/json', 'Authorization' : 'bearer' + self.accessToken}
        response = requests.request('GET', url, headers=headers)

        if response.status_code != 200:
            raise Exception('-----> Failed to get current user! {} - {}'.format(response.status_code, response.text))
        else:
            print('-----> Successful')
            return response.json()

    #Replace the existing profileIds to the newly created ID's
    def updateProfiles(self, recordId):

        print("--> Updating profiles for {} classification.".format(self.classification))
        
        profiles = {'Profiles': []}
        for profile in self.profileIds:
            profiles['Profiles'].append(profile)
        url = 'https://{}/mediahaven-rest-api/v2/records/{}/profiles'.format(self.config["parameters"]["endpoint"], recordId)
        headers = {'Content-Type': 'application/json', 'Authorization': 'bearer' + self.accessToken}
        response = requests.request('PUT', url, headers=headers, data=json.dumps(profiles))
        if response.status_code != 204:
            raise Exception('Failed to update profiles for {} classification, {} - {}'.format(self.classification, response.status_code, response.text))
        else:
            print('---> Successful!')

    #Create a new classification and set the rights for it
    def createNewClassification(self):

        print('--> Creating new classification {}'.format(self.classification))
        url = 'https://{}/mediahaven-rest-api/v2/records'.format(self.config["parameters"]["endpoint"])
        headers = {'Content-Type': 'application/json', 'Authorization': 'bearer ' + self.accessToken}
        data = {"RecordType": "Classification", "Title": self.classification, "IngestSpaceId": "aa{}ce7-51d6-41db-bf44-b085a4464c22".format(str(self.orgId))}
        response = requests.request('POST', url, headers=headers, data=json.dumps(data))
        if response.status_code != 201:
            print('---> Creation failed! {} - {}'.format(response.status_code, response.text))
            raise Exception('Failed adding classification {}! {} - {}'.format(self.classification, response.status_code, response.text))
        else:
            print('---> Creation successful!')
            fragmentId = response.json()['Internal']['FragmentId']

        readPerm = []
        writePerm = []

        userInfo = self.getCurrentUser()
        for group in userInfo['Groups']:
            if group['Type'] == 'Administrator':
                writePerm.append(group['Id'])
            readPerm.append(group['Id'])

        print('---> Setting default rights:')
        metadata = {'Metadata':{'MergeStrategies':{'Permissions': 'OVERWRITE'}, "RightsManagement": {'Permissions': {'Read' : readPerm, 'Export': [], 'Write': writePerm}}}}
        url = 'https://{}/mediahaven-rest-api/v2/records/{}'.format(self.config["parameters"]["endpoint"], fragmentId)
        headers = {'Content-Type': 'application/json', 'Authorization': 'bearer' + self.accessToken}

        response = requests.request('POST', url, headers=headers, data=json.dumps(metadata))
        if response.status_code != 201 and response.status_code != 200:
            print('----> Failed: Rights not correctly set. {} - {}'.format(response.status_code, response.text))
        else:
            print('----> Successful!')

    #Check if a classification already exists based on name and organisation
    def checkIfClassificationExists(self, classificationName):

        print("Checking if {} is an existing classification:".format(classificationName))
        url = 'https://{}/mediahaven-rest-api/v2/records?q=%2B(Title:"{}")%20%2B(RecordType:Classification)%20%2B(IsInIngestspace:*)%20%2B(OrganisationName:"{}")'.format(self.config["parameters"]["endpoint"], self.classification, self.organisation)
        headers = {'Content-Type': 'application/json', 'Authorization': 'bearer ' + self.accessToken}
        response = requests.request('GET', url, headers=headers)

        if response.status_code != 200:
            raise Exception('Failed to query classifications, {} - {}!'.format(response.status_code, response.text))
        if response.json()['TotalNrOfResults'] == 0:
            print('-> {} is not an existing classification!'.format(classificationName))
            return 0
        else:
            return response.json()['Results'][0]['Internal']['RecordId']
  
    def updateOrCreateClassification(self):
        
        retries = 0
        while retries != 5:
            recordId = self.checkIfClassificationExists(self.classification)
            if recordId == 0:
                self.createNewClassification()
                print("Waiting for 5 seconds for classification to be made.")
                time.sleep(5)
                retries += 1
            else:
                print('-> Classification found:')
                self.updateProfiles(recordId)
                break
            if retries == 5:
                print("Failed to make classification for {}".format(self.classification))
                exit()
                
        
