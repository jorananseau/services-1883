import json
import os
import re
import sys
import openpyxl
import psycopg2
import warnings
import requests

profiles = {}
baseColumns = ["field_definition_id", "parent_definition_id", "key", "type", "index",
               "global", "advanced_search", "read_only", "index_name", "possible_values", "inheritance"]
requiredColumns = [0, 1, 2, 5, 10, 12, 13, 15, 16, 17, 18]
childsRequired = ['Trefwoorden', 'Multi select',
                 'Multi-item', 'Herhalend veld']
index = 3
orgId = 0
organisation = ""
prefix = ""
endpoint = ""
access_token = ""
config = {}

#HELPER methods#
def DB_connect():
    try:
        global config, cursor
        conn = psycopg2.connect(database=config['database']['dbname'], user=config['database']['user'], password=config['database']['password'] , host=config['database']['host'], port=config['database']['port'])
        conn.autocommit = False
        print('Connected to database!')
        return conn
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        exit()

def camelcase(string):
    return re.sub(r"(_|-)+", " ", string).title().replace(" ", "").replace("(", "").replace(")", "").replace("/", "").replace("\\", "")

def parse_bool(string):
    return True if string.upper().strip() in ['JA', 'READ-ONLY'] else False

def get_organisation_id(cursor):
    global orgId
    try:
        cursor.execute("SELECT organisation_id FROM organisations WHERE name = %s;", [organisation])
        result = cursor.fetchone()
        if result is None:
            print(f"Failed to get organisation_id for organisation: {organisation}.")
            exit()
        orgId = result[0]
    except(Exception, psycopg2.DatabaseError) as error:
        print(error)
        exit()

def get_flatkey_id(cursor, flatkey):
    id = None
    try:
        cursor.execute('SELECT field_definition_id FROM field_definitions WHERE key = %s', [flatkey])
        id = cursor.fetchone()[0]
    except(Exception, psycopg2.DatabaseError) as error:
        print(f'Error getting id for standardfield {flatkey}')
        print(error)
        exit()
    return id

def load_config():
    try:
        with open ('config.json', 'r') as f:
            return json.load(f)
    except:
        print('Error loading config file!')
        exit()

#Creating Field_definitions#
def process_excel(inputFile):
    try:
        wb = openpyxl.load_workbook(inputFile)
        worksheet = wb.worksheets[0]
        return worksheet
    except(Exception) as error:
        print(f"Error loading Excel file: {inputFile}")
        print(error)

def determine_fieldtype(type):
    if type.lower().strip() == "vrije tekst":
        return "SimpleField"
    if type.lower().strip() == "tekstvak":
        return "TextField"
    if type.lower().strip() == "single select":
        return "EnumField"
    if type.lower().strip() in ["multi select", "herhalend veld", "trefwoorden"]:
        return "ListField"
    if type.lower().strip() == "multi-item":
        return "MultiItemField"
    if type.lower().strip() == "datum":
        return "DateField"
    if type.lower().strip() == "checkbox":
        return "BooleanField"
    if type.lower().strip() == "coördinaten":
        return "GeoCoordinateField"
    if type.lower().strip() == "getal":
        return "LongField"
    if type.lower().strip() == "timecode":
        return "TimeCodeField"
    if type.lower().strip() == "edtf-datum":
        return "EDTFField"
    return "Unknown"

def row_validation(row):
    errorMessage = ''
    valid = True
    for column in requiredColumns:
        if row[column] is None:
            valid = False
            errorMessage = 'is missing required information!'
    if row[10] in ["Trefwoorden", "Multi-item", "Multi select", "Herhalend veld", "Single select"] and row[5] != 'Ja':
        if row[11] == None:
            valid = False
            errorMessage = 'is missing child values!'
    if determine_fieldtype(row[10]) == 'Unknown':
        valid = False
        errorMessage = 'has an unknown field definition type!'
    if row[5] == 'Ja' and (row[6] is None or row[6] == ''):
        valid = False
        errorMessage = 'is a standard field but has no flatkey!'
    return (valid, errorMessage)

def get_labels(row, startIndex):
    dict = {'nl_BE' : '', 'fr_BE' : '', 'en_US' : ''}
    for item in dict.keys():
        dict[item] = row[startIndex]
        if row[startIndex] is None:
            if dict['nl_BE']:
                dict[item] = dict['nl_BE']
            else:
                dict[item] = "/"
        startIndex += 1
    return dict

def get_values(row):
    values = {}
    values['parent'] = ''
    values['fieldDefinitionId'] = ''
    values['labels'] = get_labels(row, 2)
    values['standard'] = parse_bool(row[5])
    values['descriptions'] = get_labels(row, 7)
    values['type'] = row[10]
    values['possible_values'] = row[11]
    values['readonly'] = parse_bool(row[12])
    values['inheritance'] = "'Propagation'" if row[13] == 'Ja' else 'NULL'
    values['required'] = parse_bool(row[14])
    values['global'] = parse_bool(row[15])
    values['advanced'] = parse_bool(row[16])
    values['filter'] = parse_bool(row[17])
    values['portal'] = parse_bool(row[18])
    values['index'] = True
    values['flatkey'] = row[6] if row[6] is not None or row[6] != '' else ''
    return values

def insert_fieldDefinition(values, file,cursor):
    fieldId = None
    columns = ", ".join(baseColumns)
    id = "(SELECT MAX(field_definition_id) + 1 from field_definitions)"
    parentId = "NULL" if values['parent'] == '' else f"(SELECT field_definition_id from field_definitions WHERE key = '{values['parent']}')"
    key = f"'{prefix + camelcase(values['labels']['nl_BE'])}'"
    type = f"'{values['type']}'"
    index = f"{values['index']}".upper()
    global_search = f"{values['global']}".upper()
    advanced_search = f"{values['advanced']}".upper()
    read_only = f"{values['readonly']}".upper()
    index_name = f"'{organisation}'"
    possible_values = "'{" + values['possible_values'] + "}'" if values['possible_values'] is not None else "'{}'"
    inheritance = f"{values['inheritance']}"
    customValues = [id, parentId, key, type, index, global_search, advanced_search, read_only, index_name, possible_values, inheritance]
    query = "INSERT INTO field_definitions (" + columns + ") VALUES (" + ", ".join(customValues) + ") RETURNING field_definition_id;"
    print(query, file=file)
    try:
        cursor.execute(query)
        fieldId = cursor.fetchone()[0]
    except(Exception, psycopg2.DatabaseError) as error:
        print(f"Failed to insert field_definition: {key}")
        print(error)
        exit()
    insert_translations(values['labels'], file, cursor)
    return fieldId

def insert_translations(labels, file, cursor):
    base_label = labels['nl_BE']
    for key in labels:
        if not labels[key]:
            labels[key] = base_label
        query = f"INSERT INTO field_translations VALUES ((SELECT field_definition_id from field_definitions WHERE key = '{prefix + camelcase(labels['nl_BE'])}'), '{key}', '{labels[key]}');"
        print(query, file=file)
        try:
            cursor.execute(query)
        except(Exception, psycopg2.DatabaseError) as error:
            print(f"Failed to insert translation for {base_label}")
            print(error)
            exit()

def add_childFields(parentField, parentLabel, type, currentProfile, file,cursor):
    for item in parentField['possible_values'].split(','):
        if type == 'EnumField':
            key = camelcase(item[0:item.find(':')])
            profiles[currentProfile][key] = parentField.copy()
            values = item[item.find(":")+1:len(item)].split(';')
            profiles[currentProfile][key]['possible_values'] = ', '.join([x.strip() for x in values])
        else:
            key = camelcase(item.strip())
            profiles[currentProfile][key] = parentField.copy()
            profiles[currentProfile][key]['possible_values'] = '' 
        profiles[currentProfile][key]['parent'] = parentLabel
        profiles[currentProfile][key]['labels'] = {'nl_BE': key, 'fr_BE': key, 'en_US': key}
        profiles[currentProfile][key]['standard'] = False
        profiles[currentProfile][key]['descriptions'] = ''
        profiles[currentProfile][key]['type'] = type
        profiles[currentProfile][key]['inheritance'] = 'NULL'
        profiles[currentProfile][key]['filter'] = False
        profiles[currentProfile][key]['portal'] = False
        profiles[currentProfile][key]['fieldDefinitionId'] = insert_fieldDefinition(profiles[currentProfile][key], file, cursor)

def add_parentField(profile, type, file, cursor):
    profile['possible_values'] = ''
    profile['global'] = False
    profile['advanced'] = False
    profile['readonly'] = False
    profile['index'] = False
    profile['type'] = determine_fieldtype(type)
    profile['fieldDefinitionId'] = insert_fieldDefinition(profile, file, cursor)
        
def process_data(worksheet, con):
    global index
    with con: 
        with con.cursor() as cursor: #Every execute now happens inside a transaction
            get_organisation_id(cursor)
            with open("output/Inserts.sql", 'w', encoding='UTF-8') as file:
                for row in worksheet.iter_rows(min_row=3, max_col=19, values_only=True):
                    if row_validation(row)[0]:
                        profile = row[1]
                        if profile not in profiles.keys():
                            profiles[profile] = {}
                        profiles[profile][row[2]] = get_values(row)
                        parentCopy = profiles[profile][row[2]].copy()
                        type = profiles[profile][row[2]]['type']
                        if type in childsRequired and row[5] != 'Ja':
                            add_parentField(profiles[profile][row[2]], type, file, cursor)
                            if type == "Multi select":
                                add_childFields(parentCopy, prefix + camelcase(row[2]), 'EnumField', profile, file, cursor)
                            if type in ["Trefwoorden", "Herhalend veld (CommentList)"]:
                                add_childFields(parentCopy, prefix + camelcase(row[2]), 'SimpleField', profile, file, cursor)
                            if type == "Multi-item":
                                add_childFields(parentCopy, prefix + camelcase(row[2]), 'SimpleField', profile, file, cursor)
                        elif row[5] == 'Ja':
                            profiles[profile][row[2]]['fieldDefinitionId'] = get_flatkey_id(cursor, profiles[profile][row[2]]['flatkey'])
                            profiles[profile][row[2]]['type'] = determine_fieldtype(type)
                        else:
                            profiles[profile][row[2]]['type'] = determine_fieldtype(type)
                            profiles[profile][row[2]]['fieldDefinitionId'] = insert_fieldDefinition(profiles[profile][row[2]], file, cursor)
                        index += 1
                    else:
                        print(f'Row {index} ' + row_validation(row)[1])
                        exit()
    con.close()
    print("--> All inserts OK!")
    print("Database connection closed!")

def reindex():
    if access_token != "":
        url = "https://{}/mediahaven-rest-api/v2/fielddefinitions/refreshAll".format(endpoint)
        headers = {'Authorization': 'bearer ' + access_token}
        r = requests.post(url=url, headers=headers)
        if r.status_code == 202:
            print("Refreshing field definitions!")
        else:
            print("Refreshing failed: {} - {}".format(r.status_code, r.text))
            exit()
    else:
        print("No access_token to perform requests")
        exit()
    
if __name__ == "__main__":
    warnings.filterwarnings('ignore', category=UserWarning, module='openpyxl')
    config = load_config()
    access_token = sys.argv[1]
    organisation = config['organisation']['name']
    prefix = config['organisation']['prefix']
    endpoint = config['parameters']['endpoint']
    connection = DB_connect()
    worksheet = process_excel("GregoryClassificatie.xlsx")
    process_data(worksheet, connection)
    reindex()
    with open('output/Profielen.json', 'w', encoding='utf-8') as file:
        json.dump(profiles, file, indent=4)
