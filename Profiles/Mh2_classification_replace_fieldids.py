import sys
import json

import mediahaven_rest_v2.rest_client
from mediahaven_rest_v2.api.records import Records
from mediahaven_rest_v2.api.settings import Settings
from mediahaven_rest_v2.api.fielddefinitions import FieldDefinitions


if __name__ == '__main__':
        
    AP = mediahaven_rest_v2.rest_client.RestClient(sys.argv[1])
    if(len(sys.argv) == 6):
        AP.configureAuth(sys.argv[1], sys.argv[2], sys.argv[3])
        arg_offset = 4
    else:
        AP.configureAuth(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
        arg_offset = 6

    recordsEP = Records(AP)
    settingsEP = Settings(AP)
    fielddefEP = FieldDefinitions(AP)

    #Load all the field definitions
    ret = fielddefEP.list(True)
    if(ret.status != 200):
        print(" Error: Could not query field definitions!")
        sys.exit()

    #Load the file
    with open(sys.argv[arg_offset].strip(), 'r') as jsonFile:
        data = jsonFile.read()
    profile = json.loads(data)
    
    #Process the fields in the profile
    
    for field in profile['Fields']:
        for fielddef in ret.json.results:
            if field['FlatKey'] == fielddef['FlatKey']:
                field['FieldDefinitionId'] = fielddef['FieldDefinitionId']
                print(' -> mapped field with FlatKey \'{}\' to FieldDefinitionId {}'.format(field['FlatKey'], fielddef['FieldDefinitionId']))
                del field['FlatKey']
                break
        
        if('FlatKey' in field):
            print("ERROR: could not determine mapping for FlatKey \'{}\'".format(field['FlatKey']))

    with open(sys.argv[arg_offset + 1].strip(), 'w') as outfile:
        outfile.write(json.dumps(profile))
    #print(json.dumps(profile))