import requests
import json
TOKEN = ""
profiles = ""

PROFILEID = []
with open('config.json', 'r') as f:
    config = json.load(f)

with open('../FieldDefinitions/output/Profielen.json', 'r') as f:
    profiles = json.load(f)


def getToken():
    global TOKEN
    URL = "https://stage.mediahaven.com/auth/ropc.php"
    data = {"grant_type": "password", "client_id": config['client_id'], "client_secret": config[
        'client_secret'], "username": config['username'], "password": config['password']}
    r = requests.post(url=URL,  data=data)
    response = json.loads(r.content.decode('utf-8'))
    TOKEN = str(response['access_token'])


def createProfile(profile):
    URL = "https://stage.mediahaven.com/mediahaven-rest-api/v2/profiles"
    headers = {'Authorization': 'bearer ' + TOKEN}
    r = requests.post(url=URL, headers=headers, data=profile)
    response = json.loads(r.content)
    print(response["Id"])
    PROFILEID.append(response['Id'])
    print('status code: ' + str(r.status_code))


def transformJSON(profile, profileName):
    file = dict()
    file = {"Names": [{
                "Lang":  "fr_BE",
                "Value": profileName
            },
                {
                "Lang": "en_US",
                "Value": profileName
            },
                {
                "Lang": "nl_BE",
                "Value": profileName
            }
            ],
                "OrganisationId": config["Organisation_id"],
                "Fields": [],
            "RecordTypes": ["Record", "Mh2Collection"],    
            }
    fieldDef = []
    for data in profile:
        if profile[data]["parent"] == "":
            print(profile[data]["fieldDefinitionId"])
            fieldDef.append({
                    "FieldDefinitionId": profile[data]["fieldDefinitionId"],
                    "Required": profile[data]["required"],
                    "ReadOnly": profile[data]["readonly"],
                    "Labels": [
                        {
                            "Lang":  "fr_BE",
                            "Value": profile[data]["labels"]["fr_BE"]
                        },
                        {
                            "Lang": "en_US",
                            "Value": profile[data]["labels"]["en_US"]
                        },
                        {
                            "Lang": "nl_BE",
                            "Value": profile[data]["labels"]["nl_BE"]
                        }
                    ],
                    "Descriptions": [
                        {
                            "Lang":  "fr_BE",
                            "Value": profile[data]["descriptions"]["fr_BE"]
                        },
                        {
                            "Lang": "en_US",
                            "Value": profile[data]["descriptions"]["en_US"]
                        },
                        {
                            "Lang": "nl_BE",
                            "Value": profile[data]["descriptions"]["nl_BE"]
                        }
                    ],
                })
            
    file['Fields'] = fieldDef       
    print(file)
    return file

def printProfileId():
    profileIds = []
    for id in PROFILEID:
        profileIds.append(id)
    print(profileIds)

if __name__ == "__main__":
    getToken()
    print(TOKEN)
    for profile in profiles:
        # print(profiles[profile])
        print("test")
        file = transformJSON(profiles[profile], profile)
        createProfile(json.dumps(file))
    printProfileId()
